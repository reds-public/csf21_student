	audio_config u0 (
		.audio_and_video_config_0_clk_clk                            (<connected-to-audio_and_video_config_0_clk_clk>),                            //                    audio_and_video_config_0_clk.clk
		.audio_and_video_config_0_reset_reset                        (<connected-to-audio_and_video_config_0_reset_reset>),                        //                  audio_and_video_config_0_reset.reset
		.audio_and_video_config_0_avalon_av_config_slave_address     (<connected-to-audio_and_video_config_0_avalon_av_config_slave_address>),     // audio_and_video_config_0_avalon_av_config_slave.address
		.audio_and_video_config_0_avalon_av_config_slave_byteenable  (<connected-to-audio_and_video_config_0_avalon_av_config_slave_byteenable>),  //                                                .byteenable
		.audio_and_video_config_0_avalon_av_config_slave_read        (<connected-to-audio_and_video_config_0_avalon_av_config_slave_read>),        //                                                .read
		.audio_and_video_config_0_avalon_av_config_slave_write       (<connected-to-audio_and_video_config_0_avalon_av_config_slave_write>),       //                                                .write
		.audio_and_video_config_0_avalon_av_config_slave_writedata   (<connected-to-audio_and_video_config_0_avalon_av_config_slave_writedata>),   //                                                .writedata
		.audio_and_video_config_0_avalon_av_config_slave_readdata    (<connected-to-audio_and_video_config_0_avalon_av_config_slave_readdata>),    //                                                .readdata
		.audio_and_video_config_0_avalon_av_config_slave_waitrequest (<connected-to-audio_and_video_config_0_avalon_av_config_slave_waitrequest>), //                                                .waitrequest
		.audio_and_video_config_0_external_interface_SDAT            (<connected-to-audio_and_video_config_0_external_interface_SDAT>),            //     audio_and_video_config_0_external_interface.SDAT
		.audio_and_video_config_0_external_interface_SCLK            (<connected-to-audio_and_video_config_0_external_interface_SCLK>)             //                                                .SCLK
	);

