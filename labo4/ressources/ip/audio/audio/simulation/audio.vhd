-- audio.vhd

-- Generated using ACDS version 17.0 595

library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;

entity audio is
	port (
		audio_0_avalon_left_channel_sink_data     : in  std_logic_vector(15 downto 0) := (others => '0'); --    audio_0_avalon_left_channel_sink.data
		audio_0_avalon_left_channel_sink_valid    : in  std_logic                     := '0';             --                                    .valid
		audio_0_avalon_left_channel_sink_ready    : out std_logic;                                        --                                    .ready
		audio_0_avalon_left_channel_source_ready  : in  std_logic                     := '0';             --  audio_0_avalon_left_channel_source.ready
		audio_0_avalon_left_channel_source_data   : out std_logic_vector(15 downto 0);                    --                                    .data
		audio_0_avalon_left_channel_source_valid  : out std_logic;                                        --                                    .valid
		audio_0_avalon_right_channel_sink_data    : in  std_logic_vector(15 downto 0) := (others => '0'); --   audio_0_avalon_right_channel_sink.data
		audio_0_avalon_right_channel_sink_valid   : in  std_logic                     := '0';             --                                    .valid
		audio_0_avalon_right_channel_sink_ready   : out std_logic;                                        --                                    .ready
		audio_0_avalon_right_channel_source_ready : in  std_logic                     := '0';             -- audio_0_avalon_right_channel_source.ready
		audio_0_avalon_right_channel_source_data  : out std_logic_vector(15 downto 0);                    --                                    .data
		audio_0_avalon_right_channel_source_valid : out std_logic;                                        --                                    .valid
		audio_0_clk_clk                           : in  std_logic                     := '0';             --                         audio_0_clk.clk
		audio_0_external_interface_ADCDAT         : in  std_logic                     := '0';             --          audio_0_external_interface.ADCDAT
		audio_0_external_interface_ADCLRCK        : in  std_logic                     := '0';             --                                    .ADCLRCK
		audio_0_external_interface_BCLK           : in  std_logic                     := '0';             --                                    .BCLK
		audio_0_external_interface_DACDAT         : out std_logic;                                        --                                    .DACDAT
		audio_0_external_interface_DACLRCK        : in  std_logic                     := '0';             --                                    .DACLRCK
		audio_0_reset_reset                       : in  std_logic                     := '0';             --                       audio_0_reset.reset
		clk_clk                                   : in  std_logic                     := '0';             --                                 clk.clk
		reset_reset_n                             : in  std_logic                     := '0'              --                               reset.reset_n
	);
end entity audio;

architecture rtl of audio is
	component audio_audio_0 is
		port (
			clk                          : in  std_logic                     := 'X';             -- clk
			reset                        : in  std_logic                     := 'X';             -- reset
			from_adc_left_channel_ready  : in  std_logic                     := 'X';             -- ready
			from_adc_left_channel_data   : out std_logic_vector(15 downto 0);                    -- data
			from_adc_left_channel_valid  : out std_logic;                                        -- valid
			from_adc_right_channel_ready : in  std_logic                     := 'X';             -- ready
			from_adc_right_channel_data  : out std_logic_vector(15 downto 0);                    -- data
			from_adc_right_channel_valid : out std_logic;                                        -- valid
			to_dac_left_channel_data     : in  std_logic_vector(15 downto 0) := (others => 'X'); -- data
			to_dac_left_channel_valid    : in  std_logic                     := 'X';             -- valid
			to_dac_left_channel_ready    : out std_logic;                                        -- ready
			to_dac_right_channel_data    : in  std_logic_vector(15 downto 0) := (others => 'X'); -- data
			to_dac_right_channel_valid   : in  std_logic                     := 'X';             -- valid
			to_dac_right_channel_ready   : out std_logic;                                        -- ready
			AUD_ADCDAT                   : in  std_logic                     := 'X';             -- export
			AUD_ADCLRCK                  : in  std_logic                     := 'X';             -- export
			AUD_BCLK                     : in  std_logic                     := 'X';             -- export
			AUD_DACDAT                   : out std_logic;                                        -- export
			AUD_DACLRCK                  : in  std_logic                     := 'X'              -- export
		);
	end component audio_audio_0;

begin

	audio_0 : component audio_audio_0
		port map (
			clk                          => audio_0_clk_clk,                           --                         clk.clk
			reset                        => audio_0_reset_reset,                       --                       reset.reset
			from_adc_left_channel_ready  => audio_0_avalon_left_channel_source_ready,  --  avalon_left_channel_source.ready
			from_adc_left_channel_data   => audio_0_avalon_left_channel_source_data,   --                            .data
			from_adc_left_channel_valid  => audio_0_avalon_left_channel_source_valid,  --                            .valid
			from_adc_right_channel_ready => audio_0_avalon_right_channel_source_ready, -- avalon_right_channel_source.ready
			from_adc_right_channel_data  => audio_0_avalon_right_channel_source_data,  --                            .data
			from_adc_right_channel_valid => audio_0_avalon_right_channel_source_valid, --                            .valid
			to_dac_left_channel_data     => audio_0_avalon_left_channel_sink_data,     --    avalon_left_channel_sink.data
			to_dac_left_channel_valid    => audio_0_avalon_left_channel_sink_valid,    --                            .valid
			to_dac_left_channel_ready    => audio_0_avalon_left_channel_sink_ready,    --                            .ready
			to_dac_right_channel_data    => audio_0_avalon_right_channel_sink_data,    --   avalon_right_channel_sink.data
			to_dac_right_channel_valid   => audio_0_avalon_right_channel_sink_valid,   --                            .valid
			to_dac_right_channel_ready   => audio_0_avalon_right_channel_sink_ready,   --                            .ready
			AUD_ADCDAT                   => audio_0_external_interface_ADCDAT,         --          external_interface.export
			AUD_ADCLRCK                  => audio_0_external_interface_ADCLRCK,        --                            .export
			AUD_BCLK                     => audio_0_external_interface_BCLK,           --                            .export
			AUD_DACDAT                   => audio_0_external_interface_DACDAT,         --                            .export
			AUD_DACLRCK                  => audio_0_external_interface_DACLRCK         --                            .export
		);

end architecture rtl; -- of audio
