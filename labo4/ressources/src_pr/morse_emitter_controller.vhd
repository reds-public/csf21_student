-------------------------------------------------------------------------------
-- HEIG-VD, Haute Ecole d'Ingenierie et de Gestion du canton de Vaud
-- Institut REDS, Reconfigurable & Embedded Digital Systems
--
-- File         : morse_emitter_controller.vhd
-- Description  : 
--
-- Author       : Mike Meury
-- Date         : 31.03.21
-- Version      : 0.0
--
-- Dependencies : 
--
--| Modifications |------------------------------------------------------------
-- Version   Author Date               Description
-- 0.0       MIM    31.03.21           Creation
-------------------------------------------------------------------------------

------------------------
-- Standard libraries --
------------------------
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

-------------------------
-- Specifics libraries --
-------------------------

------------
-- Entity --
------------
entity morse_emitter_controller is
    generic (
        DATASIZE         : integer := 16;
        SAMPLING_DIVISOR : integer := 1600
        );
    port (
        -- standard inputs
        clk_i                    : in  std_logic;
        rst_i                    : in  std_logic;
        -- Input data
        morse_i                  : in  std_logic;
        -- Output interface to audio
        left_channel_sink_data   : out std_logic_vector(15 downto 0);
        left_channel_sink_valid  : out std_logic;
        left_channel_sink_ready  : in  std_logic;
        right_channel_sink_data  : out std_logic_vector(15 downto 0);
        right_channel_sink_valid : out std_logic;
        right_channel_sink_ready : in  std_logic
        );
end entity;  -- morse_emitter_controller

------------------
-- Architecture --
------------------
architecture behave of morse_emitter_controller is

    -----------
    -- Types --
    -----------
    type gen_array_t is array(0 to 31) of std_logic_vector(DATASIZE-1 downto 0);

    --------------
    -- Constant --
    --------------
    constant ARRAY_CST_GEN_C : gen_array_t := (
        (0)  => X"0000",
        (1)  => X"006A",
        (2)  => X"00D0",
        (3)  => X"012C",
        (4)  => X"017C",
        (5)  => X"01BB",
        (6)  => X"01E6",
        (7)  => X"01FD",
        (8)  => X"01FD",
        (9)  => X"01E6",
        (10) => X"01BB",
        (11) => X"017C",
        (12) => X"012C",
        (13) => X"00D0",
        (14) => X"006A",
        (15) => X"0000",
        (16) => X"FF95",
        (17) => X"FF2F",
        (18) => X"FED3",
        (19) => X"FE83",
        (20) => X"FE44",
        (21) => X"FE19",
        (22) => X"FE02",
        (23) => X"FE02",
        (24) => X"FE19",
        (25) => X"FE44",
        (26) => X"FE83",
        (27) => X"FED3",
        (28) => X"FF2F",
        (29) => X"FF95",
        (30) => X"FFFF",
        (31) => X"0000"
        );

    -------------
    -- Signals --
    -------------
    -- Control both channels in the same way, otherwise, the audio controller
    -- will just wait until the second channel received something but it does
    -- not matter in our context
    signal audio_rdy_s    : std_logic;
    -- Counter to browse signal generator (up to 31)
    signal cpt_data_s     : unsigned(4 downto 0) := (others => '0');
    -- Counter to sample data
    signal cpt_sampling_s : unsigned(9 downto 0);
    -- Flag to generate transaction
    signal gen_data_s     : std_logic;
    -- Data signal to audio
    signal data_audio_s   : std_logic_vector(DATASIZE-1 downto 0);
    signal data_valid_s   : std_logic;

begin  -- behave

    -- Assertions
    assert (DATASIZE = 16) report "DATASIZE unsupported" severity failure;

    -- Verify both channels are ready
    audio_rdy_s <= left_channel_sink_ready and right_channel_sink_ready;

    -- Sampling counter : This counter must generate a data every 800 clocks
    -- cycle if the clock is at 50MHz in order to generate a sinus with a 2kHz
    -- frequency
    process(clk_i, rst_i)
    begin
        if rst_i = '1' then
            cpt_sampling_s <= (others => '0');
        elsif rising_edge(clk_i) then
            if cpt_sampling_s >= SAMPLING_DIVISOR-1 then
                cpt_sampling_s <= (others => '0');
            else
                cpt_sampling_s <= cpt_sampling_s + 1;
            end if;
        end if;
    end process;

    -- Data counter in order to send all data in constant array
    process(clk_i, rst_i)
    begin
        if rst_i = '1' then
            cpt_data_s <= (others => '0');
        elsif rising_edge(clk_i) then
            if data_valid_s = '1' then         -- if a data has been generated
                cpt_data_s <= cpt_data_s + 1;  -- will overlap automatically
            end if;
        end if;
    end process;

    -- Flag to generate transaction to audio
    gen_data_s <= '1' when cpt_sampling_s = 0 else
                  '0';

    -- Flag to synchronize
    data_valid_s <= gen_data_s and audio_rdy_s;

    -- Define data
    data_audio_s <= ARRAY_CST_GEN_C(to_integer(cpt_data_s));

    -- Connect correcly outputs (mono)
    left_channel_sink_data   <= data_audio_s when morse_i = '1' else (others => '0');
    right_channel_sink_data  <= data_audio_s when morse_i = '1' else (others => '0');
    left_channel_sink_valid  <= data_valid_s;  -- still write data at desired frequency
    right_channel_sink_valid <= data_valid_s;  -- still write data at desired frequency

end architecture;  --behave

