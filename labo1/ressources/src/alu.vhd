-----------------------------------------------------------------------
-- HEIG-VD, Haute Ecole d'Ingenerie et de Gestion du Canton de Vaud
-- Institut REDS
--
-- Composant    : alu
-- Description  : Unité arithmétique et logique capable d'effectuer
--                8 opérations.
-- Auteur       : Yann Thoma
-- Date         : 13.02.2015
-- Version      : 1.0
--
-----------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity alu is
generic (
	SIZE  : integer := 8
);
port (
	a_i      : in  std_logic_vector(SIZE-1 downto 0);
	b_i      : in  std_logic_vector(SIZE-1 downto 0);
	result_o : out std_logic_vector(SIZE-1 downto 0);
	carry_i  : in std_logic;
	carry_o  : out std_logic;
	mode_i   : in  std_logic_vector(2 downto 0)
);
end alu;

architecture behave of alu is

begin

end behave;
