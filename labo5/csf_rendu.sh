#!/bin/bash

PROJECT_DIR=ressources
SRC_DIR=ressources/src
XML_FILE=ressources/morse_burst_emitter.xml
RMDB_FILE=ressources/default.rmdb
ARCHIVE=rendu.tar.gz

if [ ! -d "$PROJECT_DIR" ]
then
    echo "Could not find $PROJECT_DIR directory in $(pwd)" >&2
    exit 1
fi

if [ ! -d "$SRC_DIR" ]
then
    echo "Could not find $SRC_DIR directory in $(pwd)" >&2
    exit 1
fi

if [ ! -f "$XML_FILE" ]
then
    echo "Could not find $XML_FILE directory in $(pwd)" >&2
    exit 1
fi

if [ ! -f "$RMDB_FILE" ]
then
    echo "Could not find $RMDB_FILE directory in $(pwd)" >&2
    exit 1
fi

echo "The following files are archived in $ARCHIVE : "
tar --exclude='rendu.tar.gz' --exclude='comp' --exclude='sim' --exclude='pr' --exclude='VRMDATA' --exclude='ip' -czvf $ARCHIVE $PROJECT_DIR
