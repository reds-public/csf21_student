package help is
  function max(l,r : integer) return integer ;
end help;
package body help is
  function max(l,r : integer) return integer is begin
    if l > r then return l;
    else return r;
    end if;
  end;
end help;

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use work.help.all;

entity charToMorseController is
    generic (
      DOTPERIODS : integer := 1;
      ERRNO : integer := 0
    );
    PORT( 
        clock       :   IN  std_logic;
        reset       :   IN  std_logic;
        char        :   IN  std_logic_vector(7 DOWNTO 0);
        charValid   :   IN  std_logic;
        morseOut    :   OUT std_logic;
        busy        :   OUT std_logic;
        unknown     :   OUT std_logic
    );

end charToMorseController ;

ARCHITECTURE RTL OF charToMorseController IS
  -- sequence for characters
  type characterStateType is (
    idle, start,
    sA, sB, sC, sD, sE, sF, sG, sH, sI, sJ, sK, sL, sM, sN, sO, sP,
    sQ, sR, sS, sT, sU, sV, sW, sX, sY, sZ,
    s0, s1, s2, s3, s4, s5, s6, s7, s8, s9,
    s2a, s8a, s9a
  );
  signal characterState : characterStateType;
  
  signal isA, isB, isC, isD, isE, isF, isG, isH,
         isI, isJ, isK, isL, isM, isN, isO, isP,
         isQ, isR, isS, isT, isU, isV, isW, isX,
         isY, isZ,
         is0, is1, is2, is3, is4, is5, is6, is7,
         is8, is9 : std_logic;
         
  -- 1st FSM signalling
  signal gotoE, gotoI, gotoS, gotoH, goto5,
         gotoF,
         gotoL, gotoR,
         gotoP,
         gotoN, gotoD, gotoB, goto6,
         gotoC,
         gotoG, gotoZ, goto7,
         goto8,
         goto9,
         gotoT : std_logic;
         
  -- inter-FSM signalling
  signal sendDot, sendDash: std_logic;
                                                     
  -- sequence for morse units
  type sequencerStateType is (
    idle, high, low, longlow
  );
  signal sequencerState : sequencerStateType;
  signal storedChar : std_logic_vector(7 DOWNTO 0);

  signal count : unsigned(7 downto 0); -- Timer internal
  signal enable : std_logic; -- Timer internal
  signal startCounter : std_logic;
  signal lastStep : std_logic;
  signal counterDone : std_logic; -- From timer -> SM
  signal dashDotDone : std_logic;
  signal unitNb : unsigned(7 downto 0); -- From SM -> Timer
  
  signal dot_periods : unsigned(7 downto 0);
  signal dash_periods : unsigned(7 downto 0);
  signal silence_periods : unsigned(7 downto 0);

BEGIN

  -- ERRNO 9
  dot_periods <= to_unsigned(DOTPERIODS, unitNb'length) when (ERRNO /= 9) else to_unsigned(DOTPERIODS+1, unitNb'length);
  -- ERRNO 5
  dash_periods <= to_unsigned(DOTPERIODS*3, unitNb'length) when (ERRNO /= 5) else to_unsigned(DOTPERIODS, unitNb'length) when (ERRNO /= 9) else to_unsigned(DOTPERIODS*3+1, unitNb'length);
  silence_periods <= to_unsigned(DOTPERIODS*3, unitNb'length) when (ERRNO /= 5) else to_unsigned(DOTPERIODS, unitNb'length) when (ERRNO /= 9) else to_unsigned(DOTPERIODS*3+1, unitNb'length);
  

  ------------------------------------------------------------------------------
  -- conditions for morse units
  isA <= '1' when std_match(unsigned(storedChar), "01-0" & x"1") else '0';
  isB <= '1' when std_match(unsigned(storedChar), "01-0" & x"2") else '0';
  isC <= '1' when std_match(unsigned(storedChar), "01-0" & x"3") else '0';
  isD <= '1' when std_match(unsigned(storedChar), "01-0" & x"4") else '0';
  isE <= '1' when std_match(unsigned(storedChar), "01-0" & x"5") else '0';
  isF <= '1' when std_match(unsigned(storedChar), "01-0" & x"6") else '0';
  isG <= '1' when std_match(unsigned(storedChar), "01-0" & x"7") else '0';
  isH <= '1' when std_match(unsigned(storedChar), "01-0" & x"8") else '0';
  isI <= '1' when std_match(unsigned(storedChar), "01-0" & x"9") else '0';
  isJ <= '1' when std_match(unsigned(storedChar), "01-0" & x"A") else '0';
  isK <= '1' when std_match(unsigned(storedChar), "01-0" & x"B") else '0';
  isL <= '1' when std_match(unsigned(storedChar), "01-0" & x"C") else '0';
  isM <= '1' when std_match(unsigned(storedChar), "01-0" & x"D") else '0';
  isN <= '1' when std_match(unsigned(storedChar), "01-0" & x"E") else '0';
  isO <= '1' when std_match(unsigned(storedChar), "01-0" & x"F") else '0';
  isP <= '1' when std_match(unsigned(storedChar), "01-1" & x"0") else '0';
  isQ <= '1' when std_match(unsigned(storedChar), "01-1" & x"1") else '0';
  isR <= '1' when std_match(unsigned(storedChar), "01-1" & x"2") else '0';
  isS <= '1' when std_match(unsigned(storedChar), "01-1" & x"3") else '0';
  isT <= '1' when std_match(unsigned(storedChar), "01-1" & x"4") else '0';
  isU <= '1' when std_match(unsigned(storedChar), "01-1" & x"5") else '0';
  isV <= '1' when std_match(unsigned(storedChar), "01-1" & x"6") else '0';
  isW <= '1' when std_match(unsigned(storedChar), "01-1" & x"7") else '0';
  isX <= '1' when std_match(unsigned(storedChar), "01-1" & x"8") else '0';
  isY <= '1' when std_match(unsigned(storedChar), "01-1" & x"9") else '0';
  isZ <= '1' when std_match(unsigned(storedChar), "01-1" & x"A") else '0';
  is0 <= '1' when std_match(unsigned(storedChar), "0011" & x"0") else '0';
  is1 <= '1' when std_match(unsigned(storedChar), "0011" & x"1") else '0';
  is2 <= '1' when std_match(unsigned(storedChar), "0011" & x"2") else '0';
  is3 <= '1' when std_match(unsigned(storedChar), "0011" & x"3") else '0';
  is4 <= '1' when std_match(unsigned(storedChar), "0011" & x"4") else '0';
  is5 <= '1' when std_match(unsigned(storedChar), "0011" & x"5") else '0';
  is6 <= '1' when std_match(unsigned(storedChar), "0011" & x"6") else '0';
  is7 <= '1' when std_match(unsigned(storedChar), "0011" & x"7") else '0';
  is8 <= '1' when std_match(unsigned(storedChar), "0011" & x"8") else '0';
  is9 <= '1' when std_match(unsigned(storedChar), "0011" & x"9") else '0';
  goto5 <= is5;
  gotoH <= ish or goto5 or is4;
  gotoS <= isS or gotoH or isV or is3;
  gotoF <= isF;
  gotoI <= isI or gotoS or isU or gotoF or is2;
  gotoL <= isL;
  gotoR <= isR or gotoL;
  gotoP <= isP;
  gotoE <= isE or gotoI or isA or gotoR or isW or gotoP or isJ or is1;
  goto6 <= is6;
  gotoB <= isB or goto6;
  gotoD <= isD or gotoB or isX;
  gotoC <= isC;
  gotoN <= isN or gotoD or isK or gotoC or isY;
  goto7 <= is7;
  gotoZ <= isZ or goto7;
  gotoG <= isG or gotoZ or isQ;
  goto8 <= is8;
  goto9 <= is9;
  gotoT <= isT or gotoN or isM or gotoG or isO or goto8 or goto9 or is0;


  lastStep <= '1' when ((isA = '1' and characterState=sA) or 
              (isB = '1' and characterState=sB) or
              (isC = '1' and characterState=sC) or 
              (isD = '1' and characterState=sD) or  
              (isE = '1' and characterState=sE) or 
              (isF = '1' and characterState=sF) or 
              (isG = '1' and characterState=sG) or 
              (isH = '1' and characterState=sH) or 
              (isI = '1' and characterState=sI) or 
              (isJ = '1' and characterState=sJ) or 
              (isK = '1' and characterState=sK) or 
              (isL = '1' and characterState=sL) or  
              (isM = '1' and characterState=sM) or  
              (isN = '1' and characterState=sN) or  
              (isO = '1' and characterState=sO) or  
              (isP = '1' and characterState=sP) or  
              (isQ = '1' and characterState=sQ) or  
              (isR = '1' and characterState=sR) or  
              (isS = '1' and characterState=sS) or  
              (isT = '1' and characterState=sT) or  
              (isU = '1' and characterState=sU) or  
              (isV = '1' and characterState=sV) or  
              (isW = '1' and characterState=sW) or  
              (isX = '1' and characterState=sX) or  
              (isY = '1' and characterState=sY) or  
              (isZ = '1' and characterState=sZ) or  
              (is1 = '1' and characterState=s1) or  
              (is2 = '1' and characterState=s2) or  
              (is3 = '1' and characterState=s3) or  
              (is4 = '1' and characterState=s4) or  
              (is5 = '1' and characterState=s5) or  
              (is6 = '1' and characterState=s6) or  
              (is7 = '1' and characterState=s7) or  
              (is8 = '1' and characterState=s8) or  
              (is9 = '1' and characterState=s9)) else '0';
    
  --ERRNO 10          
  busy <= '0' when characterState = idle else '1' when (ERRNO /= 10) else '0';
  dashDotDone <= '1' when (sequencerState = high and counterDone = '1') else '0';

  --store char when valid
  storeChar : process(reset,clock)
  begin  
    if reset = '1' then
      storedChar <= (others=>'0');
    elsif rising_edge(clock) then
      if (charValid = '1') then
        storedChar <= char;
        -- ERRNO 13
        if ERRNO = 13 then
          storedChar <= (others=>'-');
        end if;
      else
        storedChar <= storedChar;
      end if;

      -- ERRNO 11
      if ERRNO = 11 then
        storedChar <= std_logic_vector(to_unsigned(48, 8));
      end if;
    end if;
  end process storeChar;

  -- sequence for morse units
  sendCharacterState: process(reset, clock)
  begin
    if reset = '1' then
      -- ERRNO 7
      if(ERRNO /= 7) then
        characterState <= idle;
      end if;
      unknown <= '0';
    elsif rising_edge(clock) then
      unknown <= '0';
      case characterState is
        -- idle
        when idle =>
          -- ERRNO 1
          if(ERRNO /= 1) then
            if charValid = '1' then
              characterState <= start;
            end if;
          end if;
          -- ERRNO 8
          if(ERRNO = 8) then
            characterState <= start;
          end if;
        -- start
        when start => 
          if gotoE = '1' then
            characterState <= sE;
          elsif gotoT = '1' then
            characterState <= sT;
            -- ERRNO 2
            if(ERRNO = 2) then 
              characterState <= sE;
            end if;
          else
            unknown <= '1';
            -- ERRNO 3
            if(ERRNO = 3) then
              unknown <= '0';
            end if;
            characterState <= idle;
          end if;
          
        -- level 1
        when sE =>
          if dashDotDone = '1' then
            if isE = '1' then
              characterState <= idle;
            elsif gotoI = '1' then
              characterState <= sI;
            else
              characterState <= sA;
            end if;
          end if;
        when sT =>
          if dashDotDone = '1' then
            if isT = '1' then
              characterState <= idle;
            elsif gotoN = '1' then
              characterState <= sN;
            else
              characterState <= sM;
            end if;
          end if;
        -- level 2
        when sI =>
          if dashDotDone = '1' then
            if isI = '1' then
              characterState <= idle;
            elsif gotoS = '1' then
              characterState <= sS;
            else
              characterState <= sU;
            end if;
          end if;
        when sA =>
          if dashDotDone = '1' then
            if isA = '1' then
              characterState <= idle;
            elsif gotoR = '1' then
              characterState <= sR;
            else
              characterState <= sW;
            end if;
          end if;
        when sN =>
          if dashDotDone = '1' then
            if isN = '1' then
              characterState <= idle;
            elsif gotoD = '1' then
              characterState <= sD;
            else
              characterState <= sK;
            end if;
          end if;
        when sM =>
          if dashDotDone = '1' then
            if isM = '1' then
              characterState <= idle;
              -- ERRNO 4
              if(ERRNO = 4) then
                characterState <= sG;
              end if;
            elsif gotoG = '1' then
              characterState <= sG;
            else
              characterState <= sO;
            end if;
          end if;
        -- level 3a
        when sS =>
          if dashDotDone = '1' then
            if isS = '1' then
              characterState <= idle;
            elsif gotoH = '1' then
              characterState <= sH;
            else
              characterState <= sV;
            end if;
          end if;
        when sU =>
          if dashDotDone = '1' then
            if isU = '1' then
              characterState <= idle;
            elsif gotoF = '1' then
              characterState <= sF;
            else
              characterState <= s2a;
            end if;
          end if;
        when sR =>
          if dashDotDone = '1' then
            if isR = '1' then
              characterState <= idle;
            elsif gotoL = '1' then
              characterState <= sL;
            else
              characterState <= idle;
            end if;
          end if;
        when sW =>
          if dashDotDone = '1' then
            if isW = '1' then
              characterState <= idle;
            elsif gotoP = '1' then
              characterState <= sP;
            else
              characterState <= sJ;
            end if;
          end if;
        -- level 3b
        when sD =>
          if dashDotDone = '1' then
            if isD = '1' then
              characterState <= idle;
            elsif gotoB = '1' then
              characterState <= sB;
            else
              characterState <= sX;
            end if;
          end if;
        when sK =>
          if dashDotDone = '1' then
            if isK = '1' then
              characterState <= idle;
            elsif gotoC = '1' then
              characterState <= sC;
            else
              characterState <= sY;
            end if;
          end if;
        when sG =>
          if dashDotDone = '1' then
            if isG = '1' then
              characterState <= idle;
            elsif gotoZ = '1' then
              characterState <= sZ;
            else
              characterState <= sQ;
            end if;
          end if;
        when sO =>
          if dashDotDone = '1' then
            if isO = '1' then
              characterState <= idle;
            elsif goto8 = '1' then
              characterState <= s8a;
            else
              characterState <= s9a;
            end if;
          end if;
        -- level 4a
        when sH =>
          if dashDotDone = '1' then
            if isH = '1' then
              characterState <= idle;
            elsif goto5 = '1' then
              characterState <= s5;
            else
              characterState <= s4;
            end if;
          end if;
        when sV =>
          if dashDotDone = '1' then
            if isV = '1' then
              characterState <= idle;
            else
              characterState <= s3;
            end if;
          end if;
        when sF =>
          if dashDotDone = '1' then
            characterState <= idle;
          end if;
        when s2a =>
          if dashDotDone = '1' then
            characterState <= s2;
          end if;
        -- level 4b
        when sL =>
          if dashDotDone = '1' then
            characterState <= idle;
          end if;
        when sP =>
          if dashDotDone = '1' then
            characterState <= idle;
          end if;
        when sJ =>
          if dashDotDone = '1' then
            if isJ = '1' then
              characterState <= idle;
            else
              characterState <= s1;
            end if;
          end if;
        -- level 4c
        when sB =>
          if dashDotDone = '1' then
            if isB = '1' then
              characterState <= idle;
            elsif goto6 = '1' then
              characterState <= s6;
            else
              characterState <= idle;
            end if;
          end if;
        when sX =>
          if dashDotDone = '1' then
            characterState <= idle;
          end if;
        when sC =>
          if dashDotDone = '1' then
            characterState <= idle;
          end if;
        when sY =>
          if dashDotDone = '1' then
            characterState <= idle;
          end if;
        -- level 4d
        when sZ =>
          if dashDotDone = '1' then
            if isZ = '1' then
              characterState <= idle;
            elsif goto7 = '1' then
              characterState <= s7;
            else
              characterState <= idle;
            end if;
          end if;
        when sQ =>
          if dashDotDone = '1' then
            characterState <= idle;
          end if;
        when s8a =>
          if dashDotDone = '1' then
            characterState <= s8;
          end if;
        when s9a =>
          if dashDotDone = '1' then
            if goto9 = '1' then
              characterState <= s9;
            else
              characterState <= s0;
            end if;
          end if;
        -- level 5
        when s5 | s4 | s3 | s2 | s1 | s6 | s7 | s8 | s9 | s0 =>
          if dashDotDone = '1' then
            characterState <= idle;
          end if;

        when others => characterState <= idle;
      end case;
    end if;
  end process sendCharacterState;

  sendCharacterOutput: process(characterState)
  begin
    sendDot <= '0';
    sendDash <= '0';
    case characterState is
      -- level 1
      when sE =>
        sendDot <= '1';
      when sT =>
        sendDash <= '1';
      -- level 2
      when sI =>
        sendDot <= '1';
      when sA =>
        sendDash <= '1';
      when sN =>
        sendDot <= '1';
      when sM =>
        sendDash <= '1';
      -- level 3a
      when sS =>
        sendDot <= '1';
      when sU =>
        sendDash <= '1';
      when sR =>
        sendDot <= '1';
      when sW =>
        sendDash <= '1';
      -- level 3b
      when sD =>
        sendDot <= '1';
      when sK =>
        sendDash <= '1';
      when sG =>
        sendDot <= '1';
      when sO =>
        sendDash <= '1';
      -- level 4a
      when sH =>
        sendDot <= '1';
      when sV =>
        sendDash <= '1';
      when sF =>
        sendDot <= '1';
      when s2a =>
        sendDash <= '1';
      -- level 4b
      when sL =>
        sendDot <= '1';
      when sP =>
        sendDot <= '1';
      when sJ =>
        sendDash <= '1';
      -- level 4c
      when sB =>
        sendDot <= '1';
      when sX =>
        sendDash <= '1';
      when sC =>
        sendDot <= '1';
      when sY =>
        sendDash <= '1';
      -- level 4d
      when sZ =>
        sendDot <= '1';
      when sQ =>
        sendDash <= '1';
      when s8a =>
        sendDot <= '1';
      when s9a =>
        sendDash <= '1';
      -- level 5
      when s5 =>
        sendDot <= '1';
      when s4 =>
        sendDash <= '1';
      when s3 =>
        sendDash <= '1';
      when s2 =>
        sendDash <= '1';
      when s1 =>
        sendDash <= '1';
      when s6 =>
        sendDot <= '1';
      when s7 =>
        sendDot <= '1';
      when s8 =>
        sendDot <= '1';
      when s9 =>
        sendDot <= '1';
      when s0 =>
        sendDash <= '1';
      when others => null;
    end case;
  end process sendCharacterOutput;

  ------------------------------------------------------------------------------
  -- sequence for morse units
  sendDotDashState: process(reset, clock)
  begin
    if reset = '1' then
      -- ERRNO 6
      if(ERRNO /= 6) then
        sequencerState <= idle;
      end if;
      startCounter <= '0';
      unitNb <= (others => '0');
      morseOut <= '0';
    elsif rising_edge(clock) then
      startCounter <= '0';
      unitNb <= (others => '0');
      morseOut <= '0';
      case sequencerState is
        -- idle
        when idle =>
          if sendDot = '1' or sendDash = '1' then
            sequencerState <= high;
            startCounter <= '1';
            morseOut <= '1';
            if sendDot = '1' then
              unitNb <= dot_periods;
            elsif sendDash = '1' then
              unitNb <= dash_periods;
            end if;
          end if;
        -- dot
        when high =>
          morseOut <= '1';
          if sendDot = '1' then
            unitNb <= dot_periods;
          elsif sendDash = '1' then
            unitNb <= dash_periods;
          end if;
          if counterDone = '1' then
            unitNb <= silence_periods;
            if lastStep = '1' then
              sequencerState <= longlow;
            else 
              sequencerState <= low;
            end if;
            morseOut <= '0';
            startCounter <= '1';
          end if;
        -- separator
        when low =>
          unitNb <= silence_periods;
          if counterDone = '1' then
            if sendDot = '1' or sendDash = '1' then
              startCounter <= '1';
              morseOut <= '1';
              --ERRNO 12
              if ERRNO /= 12 then
                sequencerState <= high;
              else
                sequencerState <= low;
              end if;
              if sendDot = '1' then
                unitNb <= dot_periods;
              elsif sendDash = '1' then
                unitNb <= dash_periods;
              end if;
            else
              sequencerState <= idle;
            end if;
          end if;

        when longlow =>
            sequencerState <= low;
          
      end case;
    end if;
  end process sendDotDashState;
  
  
  counter : process(reset, clock)
  begin
      if reset = '1' then
        enable <= '0';
        count <= (others => '0');
      elsif rising_edge(clock) then
        enable <= '0';
        if(enable = '1' or startCounter = '1') then
          enable <= '1';
          count <= count + 1;
        end if;
        if(counterDone = '1') then
          enable <= '0';
          count <= (others => '0');
        end if;
      end if;
  end process counter;

  counterDone <= '1' when (count >= unitNb-1) else '0';

END ARCHITECTURE RTL;
