
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

library morse;
use morse.morse_emitter_pkg.all;

entity morse_burst_emitter is
    generic (
        FIFOSIZE : integer := 16;
        ERRNO    : integer := 0
        );
    port (
        clk_i     : in  std_logic;
        rst_i     : in  std_logic;
        control_i : in  morse_burst_emitter_control_in_t;
        control_o : out morse_burst_emitter_control_out_t;
        morse_o   : out std_logic
        );
end morse_burst_emitter;

architecture struct of morse_burst_emitter is

begin

end struct;
