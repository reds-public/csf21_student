-------------------------------------------------------------------------------
-- HEIG-VD, Haute Ecole d'Ingenierie et de Gestion du canton de Vaud
-- Institut REDS, Reconfigurable & Embedded Digital Systems
--
-- File         : front_end_audio.vhd
-- Description  : 
--
-- Author       : Mike Meury
-- Date         : 31.05.21
-- Version      : 0.0
--
-- Dependencies : 
--
--| Modifications |------------------------------------------------------------
-- Version   Author Date               Description
-- 0.0       MIM    31.05.21           Creation
-------------------------------------------------------------------------------

------------------------
-- Standard libraries --
------------------------
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

-------------------------
-- Specifics libraries --
-------------------------

------------
-- Entity --
------------
entity front_end_audio is
    generic (
        THRESHOLD_G      : integer := 1000;
        FILTER_SAMPLES_G : integer := 3
        );
    port (
        -- standard inputs
        clk_i                 : in  std_logic;
        rst_i                 : in  std_logic;
        -- Audio interface
        right_channel_data_i  : in  std_logic_vector(15 downto 0);
        right_channel_valid_i : in  std_logic;
        right_channel_ready_o : out std_logic;
        left_channel_data_i   : in  std_logic_vector(15 downto 0);
        left_channel_valid_i  : in  std_logic;
        left_channel_ready_o  : out std_logic;
        -- Output
        morse_o               : out std_logic
        );
end entity;  -- front_end_audio

------------------
-- Architecture --
------------------
architecture behave of front_end_audio is

begin  -- behave

end architecture;  --behave
