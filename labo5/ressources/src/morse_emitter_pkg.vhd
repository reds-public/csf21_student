

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

package morse_emitter_pkg is


    type morse_burst_emitter_control_in_t is record
        char       : std_logic_vector(7 downto 0);
        load_char  : std_logic;
        send       : std_logic;
        -- 28 bits allow for more than half a second at 250 MHz
        dot_period : std_logic_vector(27 downto 0);
    end record;

    type morse_burst_emitter_control_out_t is record
        busy    : std_logic;
        full    : std_logic;
        unknown : std_logic;
    end record;

    constant MORSE_SYMBOL_LENGTH : integer := 5;

    subtype morse_char_length_t is integer range 0 to MORSE_SYMBOL_LENGTH;

    type morse_char_t is record
        size  : morse_char_length_t;
        value : std_logic_vector(0 to 4);
    end record;

    type ascii_letter_to_morse_array_t is array (integer range <>) of morse_char_t;

    constant letter_conversion_c : ascii_letter_to_morse_array_t(0 to 25) := (
        (2, "01---"),
        (4, "1000-"),
        (4, "1010-"),
        (3, "100--"),
        (1, "0----"),
        (4, "0010-"),
        (3, "110--"),
        (4, "0000-"),
        (2, "00---"),
        (4, "0111-"),
        (3, "101--"),
        (4, "0100-"),
        (2, "11---"),
        (2, "10---"),
        (3, "111--"),
        (4, "0110-"),
        (4, "1101-"),
        (3, "010--"),
        (3, "000--"),
        (1, "1----"),
        (3, "001--"),
        (4, "0001-"),
        (3, "011--"),
        (4, "1001-"),
        (4, "1011-"),
        (4, "1100-")
        );

    constant number_conversion_c : ascii_letter_to_morse_array_t(0 to 9) := (
        (5, "11111"),
        (5, "01111"),
        (5, "00111"),
        (5, "00011"),
        (5, "00001"),
        (5, "00000"),
        (5, "10000"),
        (5, "11000"),
        (5, "11100"),
        (5, "11110")
        );

    type morse_char_or_unknown_t is record
        unknown : std_logic;
        char    : morse_char_t;
    end record;

    function ascii_to_morse(ascii : std_logic_vector(7 downto 0)) return morse_char_or_unknown_t;

    function morse_to_ascii(morse : morse_char_t) return std_logic_vector;


end package;

package body morse_emitter_pkg is

    function ascii_to_morse(ascii : std_logic_vector(7 downto 0)) return morse_char_or_unknown_t is
    begin
        if (unsigned(ascii) = 32) then
            -- This is a space, and will be interpreted as such
            return ('0', (0, "-----"));
        end if;
        if (unsigned(ascii) > 64) and (unsigned(ascii) < 91) then
            return ('0', letter_conversion_c(to_integer(unsigned(ascii)) - 65));
        end if;
        if (unsigned(ascii) > 96) and (unsigned(ascii) < 123) then
            return ('0', letter_conversion_c(to_integer(unsigned(ascii)) - 97));
        end if;
        if (unsigned(ascii) > 47) and (unsigned(ascii) < 58) then
            return ('0', number_conversion_c(to_integer(unsigned(ascii)) - 48));
        end if;
        return ('1', (0, "-----"));
    end;

    function morse_to_ascii(morse : morse_char_t) return std_logic_vector is
        variable result_v : integer;
    begin
        result_v := 0;
        if morse = (0, "-----") then
            result_v := 32;
        else
            for i in number_conversion_c'range loop
                if morse.size = number_conversion_c(i).size and morse.value(0 to number_conversion_c(i).size - 1) = number_conversion_c(i).value(0 to number_conversion_c(i).size - 1) then
                    result_v := 48 + i;
                end if;
            end loop;
            for i in letter_conversion_c'range loop
                if morse.size = letter_conversion_c(i).size and morse.value(0 to letter_conversion_c(i).size - 1) = letter_conversion_c(i).value(0 to letter_conversion_c(i).size - 1) then
                    result_v := 65 + i;
                end if;
            end loop;
        end if;
        return std_logic_vector(to_unsigned(result_v, 8));
    end;

end package body;
