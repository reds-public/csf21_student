
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

library morse;
use morse.morse_pkg.all;

entity morse_char_receiver is
    generic (
        LOG_RELATIVE_MARGIN : integer := 0;
        ERRNO               : integer := 0
        );
    port (
        clk_i              : in  std_logic;
        rst_i              : in  std_logic;
        char_o             : out std_logic_vector(7 downto 0);
        char_valid_o       : out std_logic;
        unknown_o          : out std_logic;
        dot_period_error_o : out std_logic;
        morse_i            : in  std_logic;
        dot_period_i       : in  std_logic_vector(27 downto 0)
        );
end morse_char_receiver;

architecture behave of morse_char_receiver is

begin

end behave;
