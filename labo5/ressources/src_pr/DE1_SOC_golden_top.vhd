--------------------------------------------------------------------------------
-- HEIG-VD /////////////////////////////////////////////////////////////////////
-- Haute Ecole d'Ingenerie et de Gestion du Canton de Vaud
-- School of Business and Engineering in Canton de Vaud
--------------------------------------------------------------------------------
-- REDS Institute //////////////////////////////////////////////////////////////
-- Reconfigurable Embedded Digital Systems
--------------------------------------------------------------------------------
--
-- File                 : DE1_SOC_golden_top.vhd
-- Author               : Mike Meury (MIM)
-- Date                 : 17.03.2021
--
-- Context              : Project Morse
--
--------------------------------------------------------------------------------
-- Description : DE1-SoC golden top for Morse project
--   
--               
--
--------------------------------------------------------------------------------
-- Dependencies :
--   
--------------------------------------------------------------------------------
-- Modifications :
-- Ver    Date        Engineer      Comments
-- 1.0    07.03.21    MIM           Creation
--
--------------------------------------------------------------------------------

------------------------
-- Standard libraries --
------------------------
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

---------------------
-- Project library --
---------------------
library morse;
use morse.morse_emitter_pkg.all;

------------
-- Entity --
------------
entity DE1_SOC_golden_top is
    port (
        ------------------------------------------------------------------------
        -- ADC - 3.3V
        ADC_CONVST : out std_logic;
        ADC_DIN    : out std_logic;
        ADC_DOUT   : in  std_logic;
        ADC_SCLK   : out std_logic;

        ------------------------------------------------------------------------
        -- AUD - 3.3V
        AUD_ADCDATA : in    std_logic;
        AUD_ADCLRCK : inout std_logic;
        AUD_BCLK    : inout std_logic;
        AUD_DACDATA : out   std_logic;
        AUD_DACLRCK : inout std_logic;
        AUD_XCK     : out   std_logic;

        ------------------------------------------------------------------------
        -- CLOCK2
        CLOCK2_50 : in std_logic;

        ------------------------------------------------------------------------
        -- CLOCK3
        CLOCK3_50 : in std_logic;

        ------------------------------------------------------------------------
        -- CLOCK4
        CLOCK4_50 : in std_logic;

        ------------------------------------------------------------------------
        -- CLOCK
        CLOCK_50 : in std_logic;

        ------------------------------------------------------------------------
        -- DRAM - 3.3V
        DRAM_ADDR  : out std_logic_vector(12 downto 0);
        DRAM_BA    : out std_logic_vector(1 downto 0);
        DRAM_CAS_N : out std_logic;
        DRAM_CKE   : out std_logic;
        DRAM_CLK   : out std_logic;
        DRAM_CS_N  : out std_logic;
        DRAM_DQ    : in  std_logic;
        DRAM_LDQM  : out std_logic;
        DRAM_RAS_N : out std_logic;
        DRAM_UDQM  : out std_logic;
        DRAM_WE_N  : out std_logic;

        ------------------------------------------------------------------------
        -- FAN - 3.3V
        FAN_CTRL : out std_logic;

        ------------------------------------------------------------------------
        -- FPGA - 3.3V
        FPGA_I2C_SCLK : out   std_logic;
        FPGA_I2C_SDAT : inout std_logic;

        ------------------------------------------------------------------------
        -- 


        --CLOCK_50    : in    std_logic;
        TD_CLK27    : in    std_logic;
        GPIO_0      : inout std_logic_vector(35 downto 0);
        GPIO_1      : inout std_logic_vector(35 downto 0);
        HEX0        : out   std_logic_vector(6 downto 0);
        HEX1        : out   std_logic_vector(6 downto 0);
        HEX2        : out   std_logic_vector(6 downto 0);
        HEX3        : out   std_logic_vector(6 downto 0);
        HEX4        : out   std_logic_vector(6 downto 0);
        HEX5        : out   std_logic_vector(6 downto 0);
        KEY         : in    std_logic_vector(3 downto 0);
        LEDR        : out   std_logic_vector(9 downto 0);
        SW          : in    std_logic_vector(9 downto 0);
        VGA_B       : out   std_logic_vector(7 downto 0);
        VGA_BLANK_N : out   std_logic;
        VGA_CLK     : out   std_logic;
        VGA_G       : out   std_logic_vector(7 downto 0);
        VGA_HS      : out   std_logic;
        VGA_R       : out   std_logic_vector(7 downto 0);
        VGA_SYNC_N  : out   std_logic;
        VGA_VS      : out   std_logic
     --DRAM_ADDR   : out   std_logic_vector(12 downto 0);
     --DRAM_BA     : out   std_logic_vector(1 downto 0);
     --DRAM_CAS_N  : out   std_logic;
     --DRAM_CKE    : out   std_logic;
     --DRAM_CLK    : out   std_logic;
     --DRAM_CS_N   : out   std_logic;
     --DRAM_DQ     : inout std_logic_vector(15 downto 0);
     --DRAM_LDQM   : out   std_logic;
     --DRAM_RAS_N  : out   std_logic;
     --DRAM_UDQM   : out   std_logic;
     --DRAM_WE_N   : out   std_logic 
        );
end entity;  -- DE1_SOC_golden_top

architecture rtl of DE1_SOC_golden_top is

    ----------------
    -- Components --
    ----------------
    -- audio pll
    component audio_clk is
        port (
            audio_pll_0_ref_clk_clk        : in  std_logic := 'X';  -- clk
            audio_pll_0_ref_reset_reset    : in  std_logic := 'X';  -- reset
            audio_pll_0_audio_clk_clk      : out std_logic;         -- clk
            audio_pll_0_reset_source_reset : out std_logic          -- reset
            );
    end component audio_clk;

    -- Audio configuration
    component audio_config is
        port (
            audio_and_video_config_0_clk_clk                            : in    std_logic                     := 'X';  -- clk
            audio_and_video_config_0_reset_reset                        : in    std_logic                     := 'X';  -- reset
            audio_and_video_config_0_avalon_av_config_slave_address     : in    std_logic_vector(1 downto 0)  := (others => 'X');  -- address
            audio_and_video_config_0_avalon_av_config_slave_byteenable  : in    std_logic_vector(3 downto 0)  := (others => 'X');  -- byteenable
            audio_and_video_config_0_avalon_av_config_slave_read        : in    std_logic                     := 'X';  -- read
            audio_and_video_config_0_avalon_av_config_slave_write       : in    std_logic                     := 'X';  -- write
            audio_and_video_config_0_avalon_av_config_slave_writedata   : in    std_logic_vector(31 downto 0) := (others => 'X');  -- writedata
            audio_and_video_config_0_avalon_av_config_slave_readdata    : out   std_logic_vector(31 downto 0);  -- readdata
            audio_and_video_config_0_avalon_av_config_slave_waitrequest : out   std_logic;  -- waitrequest
            audio_and_video_config_0_external_interface_SDAT            : inout std_logic                     := 'X';  -- SDAT
            audio_and_video_config_0_external_interface_SCLK            : out   std_logic  -- SCLK
            );
    end component audio_config;

    -- Controller audio
    component audio is
        port (
            clk_clk                                   : in  std_logic                     := 'X';  -- clk
            reset_reset_n                             : in  std_logic                     := 'X';  -- reset_n
            audio_0_avalon_left_channel_source_ready  : in  std_logic                     := 'X';  -- ready
            audio_0_avalon_left_channel_source_data   : out std_logic_vector(15 downto 0);  -- data
            audio_0_avalon_left_channel_source_valid  : out std_logic;  -- valid
            audio_0_avalon_right_channel_source_ready : in  std_logic                     := 'X';  -- ready
            audio_0_avalon_right_channel_source_data  : out std_logic_vector(15 downto 0);  -- data
            audio_0_avalon_right_channel_source_valid : out std_logic;  -- valid
            audio_0_avalon_left_channel_sink_data     : in  std_logic_vector(15 downto 0) := (others => 'X');  -- data
            audio_0_avalon_left_channel_sink_valid    : in  std_logic                     := 'X';  -- valid
            audio_0_avalon_left_channel_sink_ready    : out std_logic;  -- ready
            audio_0_avalon_right_channel_sink_data    : in  std_logic_vector(15 downto 0) := (others => 'X');  -- data
            audio_0_avalon_right_channel_sink_valid   : in  std_logic                     := 'X';  -- valid
            audio_0_avalon_right_channel_sink_ready   : out std_logic;  -- ready
            audio_0_external_interface_ADCDAT         : in  std_logic                     := 'X';  -- ADCDAT
            audio_0_external_interface_ADCLRCK        : in  std_logic                     := 'X';  -- ADCLRCK
            audio_0_external_interface_BCLK           : in  std_logic                     := 'X';  -- BCLK
            audio_0_external_interface_DACDAT         : out std_logic;  -- DACDAT
            audio_0_external_interface_DACLRCK        : in  std_logic                     := 'X';  -- DACLRCK
            audio_0_reset_reset                       : in  std_logic                     := 'X';  -- reset
            audio_0_clk_clk                           : in  std_logic                     := 'X'  -- clk
            );
    end component audio;

    component morse_burst_emitter is
        generic (
            FIFOSIZE : integer := 16;
            ERRNO    : integer := 0
            );
        port (
            clk_i     : in  std_logic;
            rst_i     : in  std_logic;
            control_i : in  morse_burst_emitter_control_in_t;
            control_o : out morse_burst_emitter_control_out_t;
            morse_o   : out std_logic
            );
    end component morse_burst_emitter;

    component morse_emitter_controller is
        generic (
            DATASIZE         : integer := 16;
            SAMPLING_DIVISOR : integer := 1600
            );
        port (
            -- standard inputs
            clk_i                    : in  std_logic;
            rst_i                    : in  std_logic;
            -- Input data
            morse_i                  : in  std_logic;
            -- Output interface to audio
            left_channel_sink_data   : out std_logic_vector(15 downto 0);
            left_channel_sink_valid  : out std_logic;
            left_channel_sink_ready  : in  std_logic;
            right_channel_sink_data  : out std_logic_vector(15 downto 0);
            right_channel_sink_valid : out std_logic;
            right_channel_sink_ready : in  std_logic
            );
    end component morse_emitter_controller;

    component front_end_audio is
        generic (
            THRESHOLD_G      : integer := 1000;
            FILTER_SAMPLES_G : integer := 3
            );
        port (
            -- standard inputs
            clk_i                 : in  std_logic;
            rst_i                 : in  std_logic;
            -- Audio interface
            right_channel_data_i  : in  std_logic_vector(15 downto 0);
            right_channel_valid_i : in  std_logic;
            right_channel_ready_o : out std_logic;
            left_channel_data_i   : in  std_logic_vector(15 downto 0);
            left_channel_valid_i  : in  std_logic;
            left_channel_ready_o  : out std_logic;
            -- Output
            morse_o               : out std_logic
            );
    end component front_end_audio;

    component morse_char_receiver is
        generic (
            ERRNO               : integer := 0;
            LOG_RELATIVE_MARGIN : integer := 0
            );
        port (
            clk_i              : in  std_logic;
            rst_i              : in  std_logic;
            char_o             : out std_logic_vector(7 downto 0);
            char_valid_o       : out std_logic;
            unknown_o          : out std_logic;
            dot_period_error_o : out std_logic;
            morse_i            : in  std_logic;
            dot_period_i       : in  std_logic_vector(27 downto 0)
            );
    end component morse_char_receiver;

    -- Constant
    constant DOT_PERIOD_C : std_logic_vector(27 downto 0) := X"0989680";

    -- Audio internal signals
    signal left_channel_source_rdy_s    : std_logic;
    signal left_channel_source_data_s   : std_logic_vector(15 downto 0);
    signal left_channel_source_valid_s  : std_logic;
    signal right_channel_source_rdy_s   : std_logic;
    signal right_channel_source_data_s  : std_logic_vector(15 downto 0);
    signal right_channel_source_valid_s : std_logic;
    signal left_channel_sink_rdy_s      : std_logic;
    signal left_channel_sink_data_s     : std_logic_vector(15 downto 0);
    signal left_channel_sink_valid_s    : std_logic;
    signal right_channel_sink_rdy_s     : std_logic;
    signal right_channel_sink_data_s    : std_logic_vector(15 downto 0);
    signal right_channel_sink_valid_s   : std_logic;

    signal user_rst_s : std_logic;

    signal emitter_char_s      : std_logic_vector(7 downto 0);
    signal emitter_load_char_s : std_logic;
    signal emitter_send_s      : std_logic;
    signal emitter_busy_s      : std_logic;
    signal emitter_full_s      : std_logic;
    signal emitter_unknown_s   : std_logic;

    signal emitter_ctrl_in_s  : morse_burst_emitter_control_in_t;
    signal emitter_ctrl_out_s : morse_burst_emitter_control_out_t;

    signal morse_en_s       : std_logic;
    signal morse_received_s : std_logic;

    signal audio_clk_s  : std_logic;
    signal audio_rst_s  : std_logic;
    signal audio_nrst_s : std_logic;

    signal receiver_char_s             : std_logic_vector(7 downto 0);
    signal receiver_valid_s            : std_logic;
    signal receiver_unknown_s          : std_logic;
    signal receiver_dot_period_error_s : std_logic;

    signal key0_d1_s : std_logic;
    signal key0_d2_s : std_logic;
    signal key0_d3_s : std_logic;
    signal key1_d1_s : std_logic;
    signal key1_d2_s : std_logic;
    signal key1_d3_s : std_logic;

    -- User I/O wrapping
    alias user_n_rst_a : std_logic is KEY(3);

begin

    -- Input synch
    in_synch_proc : process(CLOCK_50, user_rst_s)
    begin
        if user_rst_s = '1' then
            key0_d1_s <= '0';
            key0_d2_s <= '0';
            key0_d3_s <= '0';
            key1_d1_s <= '0';
            key1_d2_s <= '0';
            key1_d3_s <= '0';
        elsif rising_edge(CLOCK_50) then
            key0_d1_s <= not KEY(0);    -- activ low
            key0_d2_s <= key0_d1_s;
            key0_d3_s <= key0_d2_s;
            key1_d1_s <= not KEY(1);    -- activ low
            key1_d2_s <= key1_d1_s;
            key1_d3_s <= key1_d2_s;
        end if;
    end process;

    user_rst_s <= not user_n_rst_a;

    -- DEBUG
    LEDR(9) <= morse_received_s;
    LEDR(8) <= '0'; -- Can add flag if needed

    emitter_char_s      <= SW(7 downto 0);
    emitter_load_char_s <= key0_d2_s and not key0_d3_s;
    emitter_send_s      <= key1_d2_s and not key1_d3_s;

    ---------------
    -- Audio PLL --
    ---------------
    audio_pll_inst : audio_clk
        port map (
            audio_pll_0_ref_clk_clk        => CLOCK_50,
            audio_pll_0_ref_reset_reset    => user_rst_s,
            audio_pll_0_audio_clk_clk      => audio_clk_s,
            audio_pll_0_reset_source_reset => audio_rst_s
            );

    audio_nrst_s <= not audio_rst_s;
    AUD_XCK      <= audio_clk_s;

    ------------------
    -- Audio config --
    ------------------
    audio_config_inst : audio_config
        port map (
            audio_and_video_config_0_clk_clk                            => CLOCK_50,
            audio_and_video_config_0_reset_reset                        => user_rst_s,
            audio_and_video_config_0_avalon_av_config_slave_address     => (others => '0'),
            audio_and_video_config_0_avalon_av_config_slave_byteenable  => (others => '0'),
            audio_and_video_config_0_avalon_av_config_slave_read        => '0',
            audio_and_video_config_0_avalon_av_config_slave_write       => '0',
            audio_and_video_config_0_avalon_av_config_slave_writedata   => (others => '0'),
            audio_and_video_config_0_avalon_av_config_slave_readdata    => open,
            audio_and_video_config_0_avalon_av_config_slave_waitrequest => open,
            audio_and_video_config_0_external_interface_SDAT            => FPGA_I2C_SDAT,
            audio_and_video_config_0_external_interface_SCLK            => FPGA_I2C_SCLK
            );

    ----------------------
    -- Audio controller --
    ----------------------
    audio_controller_inst : audio
        port map (
            clk_clk                                   => audio_clk_s,
            reset_reset_n                             => audio_nrst_s,
            audio_0_avalon_left_channel_source_ready  => left_channel_source_rdy_s,
            audio_0_avalon_left_channel_source_data   => left_channel_source_data_s,
            audio_0_avalon_left_channel_source_valid  => left_channel_source_valid_s,
            audio_0_avalon_right_channel_source_ready => right_channel_source_rdy_s,
            audio_0_avalon_right_channel_source_data  => right_channel_source_data_s,
            audio_0_avalon_right_channel_source_valid => right_channel_source_valid_s,
            audio_0_avalon_left_channel_sink_data     => left_channel_sink_data_s,
            audio_0_avalon_left_channel_sink_valid    => left_channel_sink_valid_s,
            audio_0_avalon_left_channel_sink_ready    => left_channel_sink_rdy_s,
            audio_0_avalon_right_channel_sink_data    => right_channel_sink_data_s,
            audio_0_avalon_right_channel_sink_valid   => right_channel_sink_valid_s,
            audio_0_avalon_right_channel_sink_ready   => right_channel_sink_rdy_s,
            audio_0_external_interface_ADCDAT         => AUD_ADCDATA,
            audio_0_external_interface_ADCLRCK        => AUD_ADCLRCK,
            audio_0_external_interface_BCLK           => AUD_BCLK,
            audio_0_external_interface_DACDAT         => AUD_DACDATA,
            audio_0_external_interface_DACLRCK        => AUD_DACLRCK,
            audio_0_reset_reset                       => user_rst_s,
            audio_0_clk_clk                           => CLOCK_50
            );

    -------------------
    -- Morse emitter --
    -------------------
    morse_emitter_inst : morse_burst_emitter
        generic map (
            FIFOSIZE => 16,
            ERRNO    => 0
            )
        port map (
            clk_i     => CLOCK_50,
            rst_i     => user_rst_s,
            control_i => emitter_ctrl_in_s,
            control_o => emitter_ctrl_out_s,
            morse_o   => morse_en_s
            );

    -- record adapter
    emitter_ctrl_in_s.char       <= emitter_char_s;
    emitter_ctrl_in_s.load_char  <= emitter_load_char_s;
    emitter_ctrl_in_s.send       <= emitter_send_s;
    emitter_ctrl_in_s.dot_period <= DOT_PERIOD_C;
    emitter_busy_s               <= emitter_ctrl_out_s.busy;
    emitter_full_s               <= emitter_ctrl_out_s.full;
    emitter_unknown_s            <= emitter_ctrl_out_s.unknown;

    ------------------------------
    -- Audio emitter generation --
    ------------------------------
    morse_emitter_controller_inst : morse_emitter_controller
        generic map (
            DATASIZE         => 16,
            SAMPLING_DIVISOR => 1600
            )
        port map (
            clk_i                    => CLOCK_50,
            rst_i                    => user_rst_s,
            -- Input data
            morse_i                  => morse_en_s,
            -- Output interface to audio
            left_channel_sink_data   => left_channel_sink_data_s,
            left_channel_sink_valid  => left_channel_sink_valid_s,
            left_channel_sink_ready  => left_channel_sink_rdy_s,
            right_channel_sink_data  => right_channel_sink_data_s,
            right_channel_sink_valid => right_channel_sink_valid_s,
            right_channel_sink_ready => right_channel_sink_rdy_s
            );

    -----------------------------
    -- Audio receiver analyzer --
    -----------------------------
    front_end_audio_inst : front_end_audio
        generic map (
            THRESHOLD_G      => 128,
            FILTER_SAMPLES_G => 2048
            )
        port map (
            -- standard inputs
            clk_i                 => CLOCK_50,
            rst_i                 => user_rst_s,
            -- Audio interface
            right_channel_data_i  => right_channel_source_data_s,
            right_channel_valid_i => right_channel_source_valid_s,
            right_channel_ready_o => right_channel_source_rdy_s,
            left_channel_data_i   => left_channel_source_data_s,
            left_channel_valid_i  => left_channel_source_valid_s,
            left_channel_ready_o  => left_channel_source_rdy_s,
            -- Output
            morse_o               => morse_received_s
            );

    --------------------
    -- Morse receiver --
    --------------------
    morse_char_receiver_inst : morse_char_receiver
        generic map (
            ERRNO               => 0,
            LOG_RELATIVE_MARGIN => 2
            )
        port map (
            clk_i              => CLOCK_50,
            rst_i              => user_rst_s,
            char_o             => receiver_char_s,
            char_valid_o       => receiver_valid_s,
            unknown_o          => receiver_unknown_s,
            dot_period_error_o => receiver_dot_period_error_s,
            morse_i            => morse_received_s,
            dot_period_i       => DOT_PERIOD_C
            );

    -- Register for output
    process(CLOCK_50, user_rst_s)
    begin
        if user_rst_s = '1' then
            LEDR(7 downto 0) <= (others => '0');
        elsif rising_edge(CLOCK_50) then
            if receiver_valid_s = '1' and receiver_unknown_s = '0' then
                -- Avoid CR to be printed
                if receiver_char_s /= X"0D" then  -- ASCII value of CR
                    LEDR(7 downto 0) <= receiver_char_s;
                end if;
            end if;
        end if;
    end process;

end architecture;  -- rtl
