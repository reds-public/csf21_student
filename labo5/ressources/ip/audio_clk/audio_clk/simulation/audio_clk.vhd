-- audio_clk.vhd

-- Generated using ACDS version 18.1 625

library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;

entity audio_clk is
	port (
		audio_pll_0_audio_clk_clk      : out std_logic;        --    audio_pll_0_audio_clk.clk
		audio_pll_0_ref_clk_clk        : in  std_logic := '0'; --      audio_pll_0_ref_clk.clk
		audio_pll_0_ref_reset_reset    : in  std_logic := '0'; --    audio_pll_0_ref_reset.reset
		audio_pll_0_reset_source_reset : out std_logic         -- audio_pll_0_reset_source.reset
	);
end entity audio_clk;

architecture rtl of audio_clk is
	component audio_clk_audio_pll_0 is
		port (
			ref_clk_clk        : in  std_logic := 'X'; -- clk
			ref_reset_reset    : in  std_logic := 'X'; -- reset
			audio_clk_clk      : out std_logic;        -- clk
			reset_source_reset : out std_logic         -- reset
		);
	end component audio_clk_audio_pll_0;

begin

	audio_pll_0 : component audio_clk_audio_pll_0
		port map (
			ref_clk_clk        => audio_pll_0_ref_clk_clk,        --      ref_clk.clk
			ref_reset_reset    => audio_pll_0_ref_reset_reset,    --    ref_reset.reset
			audio_clk_clk      => audio_pll_0_audio_clk_clk,      --    audio_clk.clk
			reset_source_reset => audio_pll_0_reset_source_reset  -- reset_source.reset
		);

end architecture rtl; -- of audio_clk
