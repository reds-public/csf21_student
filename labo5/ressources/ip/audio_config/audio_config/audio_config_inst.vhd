	component audio_config is
		port (
			audio_and_video_config_0_avalon_av_config_slave_address     : in    std_logic_vector(1 downto 0)  := (others => 'X'); -- address
			audio_and_video_config_0_avalon_av_config_slave_byteenable  : in    std_logic_vector(3 downto 0)  := (others => 'X'); -- byteenable
			audio_and_video_config_0_avalon_av_config_slave_read        : in    std_logic                     := 'X';             -- read
			audio_and_video_config_0_avalon_av_config_slave_write       : in    std_logic                     := 'X';             -- write
			audio_and_video_config_0_avalon_av_config_slave_writedata   : in    std_logic_vector(31 downto 0) := (others => 'X'); -- writedata
			audio_and_video_config_0_avalon_av_config_slave_readdata    : out   std_logic_vector(31 downto 0);                    -- readdata
			audio_and_video_config_0_avalon_av_config_slave_waitrequest : out   std_logic;                                        -- waitrequest
			audio_and_video_config_0_clk_clk                            : in    std_logic                     := 'X';             -- clk
			audio_and_video_config_0_external_interface_SDAT            : inout std_logic                     := 'X';             -- SDAT
			audio_and_video_config_0_external_interface_SCLK            : out   std_logic;                                        -- SCLK
			audio_and_video_config_0_reset_reset                        : in    std_logic                     := 'X'              -- reset
		);
	end component audio_config;

	u0 : component audio_config
		port map (
			audio_and_video_config_0_avalon_av_config_slave_address     => CONNECTED_TO_audio_and_video_config_0_avalon_av_config_slave_address,     -- audio_and_video_config_0_avalon_av_config_slave.address
			audio_and_video_config_0_avalon_av_config_slave_byteenable  => CONNECTED_TO_audio_and_video_config_0_avalon_av_config_slave_byteenable,  --                                                .byteenable
			audio_and_video_config_0_avalon_av_config_slave_read        => CONNECTED_TO_audio_and_video_config_0_avalon_av_config_slave_read,        --                                                .read
			audio_and_video_config_0_avalon_av_config_slave_write       => CONNECTED_TO_audio_and_video_config_0_avalon_av_config_slave_write,       --                                                .write
			audio_and_video_config_0_avalon_av_config_slave_writedata   => CONNECTED_TO_audio_and_video_config_0_avalon_av_config_slave_writedata,   --                                                .writedata
			audio_and_video_config_0_avalon_av_config_slave_readdata    => CONNECTED_TO_audio_and_video_config_0_avalon_av_config_slave_readdata,    --                                                .readdata
			audio_and_video_config_0_avalon_av_config_slave_waitrequest => CONNECTED_TO_audio_and_video_config_0_avalon_av_config_slave_waitrequest, --                                                .waitrequest
			audio_and_video_config_0_clk_clk                            => CONNECTED_TO_audio_and_video_config_0_clk_clk,                            --                    audio_and_video_config_0_clk.clk
			audio_and_video_config_0_external_interface_SDAT            => CONNECTED_TO_audio_and_video_config_0_external_interface_SDAT,            --     audio_and_video_config_0_external_interface.SDAT
			audio_and_video_config_0_external_interface_SCLK            => CONNECTED_TO_audio_and_video_config_0_external_interface_SCLK,            --                                                .SCLK
			audio_and_video_config_0_reset_reset                        => CONNECTED_TO_audio_and_video_config_0_reset_reset                         --                  audio_and_video_config_0_reset.reset
		);

