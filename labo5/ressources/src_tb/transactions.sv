/*******************************************************************************
HEIG-VD
Haute Ecole d'Ingenerie et de Gestion du Canton de Vaud
School of Business and Engineering in Canton de Vaud
********************************************************************************
REDS Institute
Reconfigurable Embedded Digital Systems
********************************************************************************

File     : transactions.sv
Author   : Yann Thoma
Date     : 20.05.2021

Context  : Verification of a Morse code receiver

********************************************************************************
Description : This file contains the transactions that are used by the
              testbench. Specifically the input transaction representing
              a Morse code.

********************************************************************************
Dependencies : -

********************************************************************************
Modifications :
Ver   Date        Person     Comments
1.0   20.05.2021  YTA        Initial version

*******************************************************************************/


`ifndef TRANSACTIONS_SV
`define TRANSACTIONS_SV

import morse_pkg::*;

import morse_sv_pkg::*;


/******************************************************************************
  Input transaction
******************************************************************************/
class MorseTransaction;

    // The type of ASCII character to constrain the randomization
    typedef enum {
        LETTER,            // A letter
        NUMBER,            // A number
        SPACE,             // A space
        CR,                // A carriage return
        UNVALID,           // An unvalid Morse code
        ANY_VALID,         // A letter, number or space
        ANY_VALID_NOSPACE, // A letter or number
        ANY,               // A letter, number, space, or unvalid Morse code
        ANY_NOSPACE        // A letter, number or unvalid Morse code
    } ascii_t;

    // log relative margin.
    // 0 by default, but can be set before randomization if required
    int log_relative_margin = 0;

    // Can be set to 1 to potentially generate errors on the send_dot_period
    bit generate_margin_error = 0;

    // Can be set to define the dot_period allowed interval
    int min_dot_period = 10;

    // Can be set to define the dot_period allowed interval
    int max_dot_period = 20;

    // Random fields
    // The type of ASCII character to send
    rand ascii_t ascii_type;
    // The ASCII character that has to be sent
    rand logic[7:0] ascii;
    // Field sent to the DUV to set its dot_period
    rand logic[27:0] dot_period;
    // Field used by the driver to generate the Morse code
    rand logic[27:0] send_dot_period;
    // The margin on the dot period
    rand int margin;
    // Maximum margin. Generated from the dot_period and the log_relative_margin
    // That's why it is rand, despite its non-randomness
    rand int max_gen_margin;

    // The Morse code to be sent
    morse_char_t morse;

    // Indicates if the Morse code is valid or not
    // Calculated in post_randomize()
    bit valid = 1;

    // Indicates if there is an error with the send_dot_period
    // Calculated in post_randomize()
    bit margin_error = 0;

    // Computes the max margin to generate, depending on the generate_margin_error
    // set by the user
    constraint max_gen_margin_c {
        generate_margin_error == 0 ->
        max_gen_margin == get_margin(dot_period, log_relative_margin);
        generate_margin_error == 1 ->
        max_gen_margin == get_margin(dot_period, log_relative_margin) + 3;
    }

    // Generates a margin within the acceptable range
    constraint margin_c {

        (margin >= -max_gen_margin) && (margin <= max_gen_margin);
    }

    // Generates the send_dot_period depending on the real DUV dot_period
    // and the margin.
    // send_dot_period can be outside the DUV margin if max_gen_margin has been
    // set accordingly.
    // Actually, send_dot_period is computed in post_randomize()
    //constraint send_dot_period_c {
    //    (send_dot_period >= dot_period - margin) && 
    //    (send_dot_period <= dot_period + margin);
    //}

    // For sure, the solver order is important here
    constraint order {
        solve dot_period before max_gen_margin;
        solve max_gen_margin before margin;
        solve margin before send_dot_period;
        solve ascii_type before ascii;
    }

    // Arbitrarily generate dot period in the [11,19] interval
    // Could be modified to 
    constraint dot_period_c {
        (dot_period >= min_dot_period) && (dot_period < max_dot_period);
    }

    // Sets the ASCII potential values depending on the type of character
    constraint ascii_c {
      (ascii_type == LETTER) -> ascii inside {[65:90]};
      (ascii_type == NUMBER) -> ascii inside {[48:57]};
      (ascii_type == SPACE) -> ascii inside {32};
      (ascii_type == CR) -> ascii inside {13};
      (ascii_type == ANY_VALID) -> ascii inside {32, [48:57], [65:90]};
      (ascii_type == ANY_VALID_NOSPACE) -> ascii inside {[48:57], [65:90]};
      (ascii_type == ANY_NOSPACE) -> ascii inside {0, [48:57], [65:90]};
      (ascii_type == ANY) -> ascii inside {0, 32, [48:57], [65:90]};
      (ascii_type == UNVALID) -> ascii inside {0};
    }

    // In post-randomization we compute the Morse code to be sent, depending
    function void post_randomize();

        morse_char_or_unknown_t va;

        // Computes the DUV margin on dot_period
        int duv_margin = get_margin(dot_period, log_relative_margin);

        // Computes the dot_period used by the driver
        send_dot_period = dot_period + margin;

        // Computes the margin_error boolean to indicate if the
        // Morse code should be correctly detected by the DUV or not
        if ((margin > duv_margin) || (margin < -duv_margin)) begin
            margin_error = 1'b1;
        end
        else begin
            /*$display("3");
            $display("Marge: %d, Margin : %d, max_gen_margin: %d", duv_margin, margin, max_gen_margin);
            $display("get_margin(): %d", get_margin(dot_period, log_relative_margin));
            $display("log_relative : %d", log_relative_margin);
            $display("dot_period : %d, send_dot_period: %d", dot_period, send_dot_period);*/
            margin_error = 1'b0;
        end

        // $display("Ascii: %b", ascii);
        if (ascii == 0) begin 
            // Send 4 dashes
            valid = 1'b0;
            morse.value = 5'b11111;
            morse.size = 4;
        end
        else begin
            valid = 1'b1;
            // Converts the ASCII to morse
            va = ascii_to_morse(ascii);
            morse = va.char;
        end
        //$display("data: %b", data);
        //$display("Size: %d", size);

    endfunction : post_randomize

endclass : MorseTransaction

class MorseReceiverOutputTrans;

  logic[7:0] ascii;
  logic valid;

endclass : MorseReceiverOutputTrans


typedef mailbox #(MorseTransaction) Morse_fifo_t;

typedef mailbox #(MorseReceiverOutputTrans) Morse_receiver_output_fifo_t;

`endif // TRANSACTIONS_SV
