-------------------------------------------------------------------------------
-- HEIG-VD, Haute Ecole d'Ingenierie et de Gestion du canton de Vaud
-- Institut REDS, Reconfigurable & Embedded Digital Systems
--
-- File         : front_end_audio_tb.vhd
-- Description  : 
--
-- Author       : Mike Meury
-- Date         : 21.05.21
-- Version      : 0.0
--
-- Dependencies : 
--
--| Modifications |------------------------------------------------------------
-- Version   Author Date               Description
-- 0.0       MIM    21.05.21           Creation
-------------------------------------------------------------------------------

------------------------
-- Standard libraries --
------------------------
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use ieee.math_real.uniform;
use ieee.math_real.floor;

-------------------------
-- Specifics libraries --
-------------------------

------------
-- Entity --
------------
entity front_end_audio_tb is
    generic (
        THRESHOLD_G         : integer := 128;  -- implemented value
        FILTER_SAMPLES_G    : integer := 5;
        SAMPLE_SIMULATION_G : integer := 100000
        );
end entity;  -- front_end_audio_tb

------------------
-- Architecture --
------------------
architecture test_bench of front_end_audio_tb is

    -- Types definition
    type sin_wave_t is array(natural range <>) of std_logic_vector(15 downto 0);

    -- test bench constants
    constant CLK_PERIOD_C : time := 10 ns;  -- 100 MHz    

    -- Define constants for generation of random durations
    constant MIN_DURATION_CYCLES_C   : integer := 32;
    constant RANGE_DURATION_CYCLES_C : integer := 256;

    -- component ports
    signal clk_sti                 : std_logic := '0';
    signal rst_sti                 : std_logic := '1';
    signal right_channel_data_sti  : std_logic_vector(15 downto 0);
    signal right_channel_valid_sti : std_logic;
    signal right_channel_ready_obs : std_logic;
    signal left_channel_data_sti   : std_logic_vector(15 downto 0);
    signal left_channel_valid_sti  : std_logic;
    signal left_channel_ready_obs  : std_logic;
    signal morse_filtered_obs      : std_logic := '0';

    -- test bench signals
    signal end_sim_s : boolean := false;

    -- Global variable becauce we need this information in multiple processes
    signal duration_sound_s : integer := 0;

    -- Sinus values
    constant sin_wave_c : sin_wave_t := (
        X"0000", X"006A", X"00D0", X"012C", X"017C", X"01BB", X"01E6", X"01FD",
        X"01FD", X"01E6", X"01BB", X"017C", X"012C", X"00D0", X"006A", X"0000",
        X"FF95", X"FF2F", X"FED3", X"FE83", X"FE44", X"FE19", X"FE02", X"FE02",
        X"FE19", X"FE44", X"FE83", X"FED3", X"FF2F", X"FF95", X"FFFF", X"0000"
        );


begin  -- architecture test_bench ----------------------------------------------

    -- component instantiation
    DUT : entity work.front_end_audio
        generic map (
            THRESHOLD_G      => THRESHOLD_G,
            FILTER_SAMPLES_G => FILTER_SAMPLES_G
            )
        port map (
            clk_i                 => clk_sti,
            rst_i                 => rst_sti,
            right_channel_data_i  => right_channel_data_sti,
            right_channel_valid_i => right_channel_valid_sti,
            right_channel_ready_o => right_channel_ready_obs,
            left_channel_data_i   => left_channel_data_sti,
            left_channel_valid_i  => left_channel_valid_sti,
            left_channel_ready_o  => left_channel_ready_obs,
            morse_o               => morse_filtered_obs
            );

    -- clock generation
    CLK_GEN : process
    begin
        clk_sti <= '1';
        wait for CLK_PERIOD_C/2;
        clk_sti <= '0';
        wait for CLK_PERIOD_C/2;
        -- Verify end of simulation
        if end_sim_s then
            wait;                       -- stop process;
        end if;
    end process;

    -- reset sequence
    RST_GEN : process
    begin
        rst_sti <= '1';
        wait for 5*CLK_PERIOD_C;
        -- resynch
        wait until rising_edge(clk_sti);
        -- deassert
        rst_sti <= '0';
        -- stop process
        wait;
    end process;

    -- Stimulus generation -- read from file
    STIM_PROC : process

        -- Variable to generate random values
        variable seed1_v                  : positive := 1;
        variable seed2_v                  : positive := 1;
        variable real_v                   : real     := 0.0;
        variable sending_sound_v          : boolean  := false;
        variable counter_sound_v          : integer  := 0;
        variable counter_duration_sound_v : integer  := 0;
        variable duration_sound_v         : integer  := 0;

        -- Function to generate random integer in a range
        impure function gen_rand_integer(
            min_value   : integer;
            range_value : integer
            ) return integer is
            -- Temp value
            variable real_v       : real := 0.0;
            variable real_min_v   : real := 0.0;
            variable real_range_v : real := 0.0;
        begin
            -- Cast parameters
            real_min_v   := real(min_value);
            real_range_v := real(range_value);
            -- Generate a real value between 0.0 and 1.0
            uniform(seed1_v, seed2_v, real_v);
            -- return integer value
            return integer(floor(real_v * real_range_v + real_min_v));
        end gen_rand_integer;

    begin

        -- Seeds
        seed1_v := 99;
        seed2_v := 99;

        -- Default value
        right_channel_data_sti  <= (others => '0');
        right_channel_valid_sti <= '0';
        left_channel_data_sti   <= (others => '0');
        left_channel_valid_sti  <= '0';

        -- Wait the end of reset sequence
        wait until rst_sti = '0';

        -- Define initial no sound time
        -- We generate a value between 8 and 64 (girlfriend's decision)
        duration_sound_v := gen_rand_integer(MIN_DURATION_CYCLES_C,
                                             RANGE_DURATION_CYCLES_C);
        -- Update signal
        duration_sound_s <= duration_sound_v;

        -- Generate data
        for i in 0 to SAMPLE_SIMULATION_G-1 loop
            -- Activ wait on both audio channel
            if (right_channel_ready_obs = '0' or
                left_channel_ready_obs = '0') then
                wait until (right_channel_ready_obs = '1' and left_channel_ready_obs = '1');
            end if;
            -- Are we already generating sound or no ?
            if sending_sound_v then
                -- Browse array of value and drive it
                right_channel_data_sti  <= sin_wave_c(counter_sound_v);
                right_channel_valid_sti <= '1';
                left_channel_data_sti   <= sin_wave_c(counter_sound_v);
                left_channel_valid_sti  <= '1';
                -- Go got next sample
                if counter_sound_v = sin_wave_c'length-1 then
                    counter_sound_v := 0;
                else
                    counter_sound_v := counter_sound_v + 1;
                end if;
                -- Verify duration
                if counter_duration_sound_v < duration_sound_v then
                    counter_duration_sound_v := counter_duration_sound_v + 1;
                else
                    -- Initialize for next duration
                    counter_duration_sound_v := 0;
                    -- We are done with generation of sound
                    sending_sound_v          := false;
                    -- We generate a value between 8 and 64 (girlfriend's decision)
                    duration_sound_v := gen_rand_integer(MIN_DURATION_CYCLES_C,
                                                         RANGE_DURATION_CYCLES_C);
                    -- Update signal
                    duration_sound_s <= duration_sound_v;
                end if;
            else
                -- Drive zeros
                right_channel_data_sti  <= (others => '0');
                right_channel_valid_sti <= '1';
                left_channel_data_sti   <= (others => '0');
                left_channel_valid_sti  <= '1';
                -- Verify duration
                if counter_duration_sound_v < duration_sound_v then
                    counter_duration_sound_v := counter_duration_sound_v + 1;
                else
                    -- Initialize for next duration
                    counter_duration_sound_v := 0;
                    -- We are done with generation of sound
                    sending_sound_v          := true;
                    -- Define new duration
                    -- We generate a value between 8 and 64 (girlfriend's decision)
                    duration_sound_v := gen_rand_integer(MIN_DURATION_CYCLES_C,
                                                         RANGE_DURATION_CYCLES_C
                                                         );
                    -- Update signal
                    duration_sound_s <= duration_sound_v;
                end if;
            end if;

            -- Resynch
            wait until rising_edge(clk_sti);
            -- Quiescant value
            right_channel_data_sti  <= (others => '0');
            right_channel_valid_sti <= '0';
            left_channel_data_sti   <= (others => '0');
            left_channel_valid_sti  <= '0';
        end loop;

        -- End of simulation
        end_sim_s <= true;
        wait;
    end process;

    VERIFICATION_PROC : process

        -- constants
        constant MAX_DEAD_TIME_C    : integer := 4*RANGE_DURATION_CYCLES_C;  -- cycles
        -- Variables
        variable watchdog           : integer := 0;
        -- Latch duration
        variable duration_latched_v : integer := 0;
        -- Local counter
        variable counter_duration_v : integer := 0;
        -- Current output
        variable DUV_out_v          : std_logic;
        -- Error counter
        variable cpt_error_v        : integer := 0;

    begin

        -- Does not verify until end of reset
        wait until rst_sti = '0';
        -- Synch
        wait until rising_edge(clk_sti);
        -- We wait until first sound has been detected
        while morse_filtered_obs = '0' loop
            -- Synch
            wait until rising_edge(clk_sti);
            --  Watchdog
            watchdog := watchdog + 1;
            if watchdog >= MAX_DEAD_TIME_C then
                report "Output is no giving us anything ... " severity error;
                -- Stop process
                wait;
            end if;
        end loop;

        -- We can consider design is generating at least something
        INFINITE_LOOP : loop
            -- Prepare counter
            counter_duration_v := 0;
            -- We consider we arrive at the beginning of sound
            duration_latched_v := duration_sound_s;
            -- Look at actual state of output
            DUV_out_v          := morse_filtered_obs;
            -- Block while stable
            while morse_filtered_obs = DUV_out_v loop
                wait until rising_edge(clk_sti);
                -- update counter
                counter_duration_v := counter_duration_v + 1;
            end loop;
            -- Verify the duration correspond for sound (2 times because we
            -- filter rising and falling edge and, in the worst case, we miss a
            -- sample in both)
            if (counter_duration_v > (duration_latched_v + 2*FILTER_SAMPLES_G)) or
                (counter_duration_v < (duration_latched_v - 2*FILTER_SAMPLES_G)) then
                report "Duration of sound is wrong, expected " &
                    integer'image(duration_latched_v) & " cycles but design generated " &
                    integer'image(counter_duration_v) & " cycles" severity error;
            end if;

        end loop;

        -- end simulation
        if end_sim_s then
            report "end of simulation ! Error counter : " & integer'image(cpt_error_v) severity note;
            wait;
        end if;

    end process;

end architecture;  --test_bench ------------------------------------------------

