/*******************************************************************************
HEIG-VD
Haute Ecole d'Ingenerie et de Gestion du Canton de Vaud
School of Business and Engineering in Canton de Vaud
********************************************************************************
REDS Institute
Reconfigurable Embedded Digital Systems
********************************************************************************

File     : scoreboard.sv
Author   : Yann Thoma
Date     : 20.05.2021

Context  : Verification of a Morse code receiver

********************************************************************************
Description : This file contains the scoreboard responsible to handle the
              input and output transactions and generate errors in case
              characters do not match.

********************************************************************************
Dependencies : -

********************************************************************************
Modifications :
Ver   Date        Person     Comments
1.0   20.05.2021  YTA        Initial version

*******************************************************************************/

`ifndef SCOREBOARD_SV
`define SCOREBOARD_SV


class Scoreboard;

    int testcase;
    
    Morse_fifo_t sequencer_to_scoreboard_fifo;
    Morse_receiver_output_fifo_t monitor_to_scoreboard_fifo;

    task run;
        automatic MorseTransaction morse_trans;
        automatic MorseReceiverOutputTrans out_trans;

        $display("Scoreboard : Start");


        while (1) begin
            // Get a transaction from the sequencer
            sequencer_to_scoreboard_fifo.get(morse_trans);

            // If we expect the DUV not to handle the character sent, then directly go to the next
            // transaction
            if (morse_trans.margin_error) begin
                // $warning("Scoreboard got a margin_error input transaction");
                continue;
            end

            // Get a transaction from the output monitor
            monitor_to_scoreboard_fifo.get(out_trans);

            // If the Morse code was an unvalid code (4 dashes for instance)
            if (!morse_trans.valid) begin
                // Check that the output monitor got an unknown
                if (morse_trans.valid != out_trans.valid) begin
                    $error("Scoreboard : Bad unknown. Sent data %b. Expected valid %b, Got %b", morse_trans.morse.value, morse_trans.valid, out_trans.valid);
                end
            end
            else if (is_lowercase(out_trans.ascii)) begin
                // THe DUV is not suppposed to send lower case letters
                $error("Scoreboard : Got a lower case. Expected ASCII %b, Got %b. Expected letter %s, Got %s", morse_trans.ascii, out_trans.ascii, ascii_to_string(morse_trans.ascii), ascii_to_string(out_trans.ascii));
            end
            // Check that everything is fine
            else if (to_lowercase(morse_trans.ascii) != to_lowercase(out_trans.ascii)) begin
                // The input and output do not match
                // Different message in case of a carriage return
                if (morse_trans.ascii == 13) begin
                    $error("Scoreboard : Expected A carriage return, Got %b. Expected CR, Got %s", out_trans.ascii, ascii_to_string(out_trans.ascii));
                end
                else begin
                    $error("Scoreboard : Expected ASCII %b, Got %b. Expected letter %s, Got %s", morse_trans.ascii, out_trans.ascii, ascii_to_string(morse_trans.ascii), ascii_to_string(out_trans.ascii));
                end
            end
            else begin
                $display("Scoreboard : Happy with letter: %s", ascii_to_string(morse_trans.ascii));
            end
        end

        $display("Scoreboard : End");
    endtask : run

endclass : Scoreboard

`endif // SCOREBOARD_SV
