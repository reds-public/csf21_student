/*******************************************************************************
HEIG-VD
Haute Ecole d'Ingenerie et de Gestion du Canton de Vaud
School of Business and Engineering in Canton de Vaud
********************************************************************************
REDS Institute
Reconfigurable Embedded Digital Systems
********************************************************************************

File     : sequencer.sv
Author   : Yann Thoma
Date     : 20.05.2021

Context  : Verification of a Morse code receiver

********************************************************************************
Description : This file contains the sequencer responsible to generate
              scenarios for each testcase.

********************************************************************************
Dependencies : -

********************************************************************************
Modifications :
Ver   Date        Person     Comments
1.0   20.05.2021  YTA        Initial version

*******************************************************************************/

`ifndef SEQUENCER_SV
`define SEQUENCER_SV

class Sequencer;

    // The testcase to be executed
    int testcase;

    // The LOG_RELATIVE_MARGIN, from the testbench generic parameter
    int log_relative_margin;
    
    // The FIFO from sequencer to driver
    Morse_fifo_t sequencer_to_driver_fifo;

    // The FIFO from sequencer to scoreboard
    Morse_fifo_t sequencer_to_scoreboard_fifo;

    // Sends the 26 letters, in alphabetical order, with no space
    task testcase_letters;
        automatic MorseTransaction packet;
        $display("Sequencer : start testcase_letters");

        for(int i = 0; i < 26; i++) begin
            packet = new;
            // This constraint on ascii defines exactly the character
            void'(packet.randomize() with{ascii == 65 + i; });
            sequencer_to_driver_fifo.put(packet);
            sequencer_to_scoreboard_fifo.put(packet);
            // $display("Sequencer: I sent a letter : %s!!!!", ascii_to_string(packet.ascii));
        end

    endtask : testcase_letters

    // Sends the 10 numbers, in order, with no space
    task testcase_numbers;
        automatic MorseTransaction packet;
        $display("Sequencer : start testcase_numbers");

        for(int i = 0; i < 10; i++) begin
            packet = new;
            void'(packet.randomize() with{ascii == 48 + i; });
            sequencer_to_driver_fifo.put(packet);
            sequencer_to_scoreboard_fifo.put(packet);
            // $display("Sequencer: I sent a letter : %s!!!!", ascii_to_string(packet.ascii));
        end

    endtask : testcase_numbers

    // Sends 10 letters, and between each pair, sends an unvalid Morse code
    task testcase_unvalid;
        automatic MorseTransaction packet;
        $display("Sequencer : start testcase_unvalid");

        for(int i = 0; i < 10; i++) begin
            // Send a letter
            packet = new;
            void'(packet.randomize() with{ascii_type == LETTER; });

            sequencer_to_driver_fifo.put(packet);
            sequencer_to_scoreboard_fifo.put(packet);

            $display("Sequencer: I sent a letter : %s!!!!", ascii_to_string(packet.ascii));

            // Send an unvalid
            packet = new;
            void'(packet.randomize() with{ascii_type == UNVALID; });

            sequencer_to_driver_fifo.put(packet);
            sequencer_to_scoreboard_fifo.put(packet);

            $display("Sequencer: I sent an unvalid Morse code!!!!");
        end

    endtask : testcase_unvalid

    // Sends the 26 letters in alphabetical order, with a space between each pair
    task testcase_space;
        automatic MorseTransaction packet;
        $display("Sequencer : start testcase_space");

        for(int i = 0; i < 26; i++) begin
            packet = new;
            void'(packet.randomize() with{ascii == 65 + i; });

            sequencer_to_driver_fifo.put(packet);
            sequencer_to_scoreboard_fifo.put(packet);

            $display("Sequencer: I sent a letter : %s!!!!", ascii_to_string(packet.ascii));

            if (i == 25) begin
                // To ensure we do not finish with a space
                break;
            end

            // Send a space
            packet = new;
            // 32 is the ASCII code of space
            void'(packet.randomize() with{ascii == 32; });
            sequencer_to_driver_fifo.put(packet);
            sequencer_to_scoreboard_fifo.put(packet);
            $display("Sequencer: I sent a space!!!!");
        end

    endtask : testcase_space

    // Sends the 26 letters in alphabetical order, and a carriage return between each pair
    task testcase_cr;
        automatic MorseTransaction packet;
        $display("Sequencer : start directed testcase");

        for(int i = 0; i < 26; i++) begin
            packet = new;
            void'(packet.randomize() with{ascii == 65 + i; });

            sequencer_to_driver_fifo.put(packet);
            sequencer_to_scoreboard_fifo.put(packet);

            $display("Sequencer: I sent a letter : %s!!!!", ascii_to_string(packet.ascii));

            if (i == 25) begin
                // To ensure we do not finish with a space
                break;
            end

            // Send a carriage return (ASCII code 13)
            packet = new;
            void'(packet.randomize() with{ascii == 13; });
            sequencer_to_driver_fifo.put(packet);
            sequencer_to_scoreboard_fifo.put(packet);
            $display("Sequencer: I sent a space!!!!");
        end

    endtask : testcase_cr

    // Sends the 26 letters in alphabetical order, but with errors on the dot period
    // Some transactions will be generated with an unvalid dot_period to check how
    // the system behaves
    task testcase_margin;
        automatic MorseTransaction packet;
        $display("Sequencer : start directed testcase");

        for(int i = 0; i < 26; i++) begin
            real margin;
            packet = new;
            // Deactivate constraints
            packet.log_relative_margin = log_relative_margin;
            // This set indicates we can generate wrong dot_periods
            packet.generate_margin_error = 1'b1;
            void'(packet.randomize() with{ascii == 65 + i; dot_period == 15;});
            sequencer_to_driver_fifo.put(packet);
            sequencer_to_scoreboard_fifo.put(packet);

            $display("Sequencer: I sent a letter : %s!!!!", ascii_to_string(packet.ascii));
        end
    endtask : testcase_margin

    // Sends 100 random letters, including spaces
    task testcase_full_random;
        automatic MorseTransaction packet;
        $display("Sequencer : start");

        for(int i = 0; i < 100; i++) begin

            packet = new;
            void'(packet.randomize() with {ascii_type == ANY; dot_period == 15;});
            sequencer_to_driver_fifo.put(packet);
            sequencer_to_scoreboard_fifo.put(packet);

            $display("Sequencer: I sent a letter : %s!!!!", ascii_to_string(packet.ascii));
        end

    endtask : testcase_full_random

    // Task executed by the sequencer.
    // Depending on the testcase we run all the tests, or only a specific one
    task run;
        $display("Sequencer : start");
        case (testcase)
        0:begin
            testcase_letters;
            testcase_numbers;
            testcase_unvalid;
            testcase_space;
            // This test does not pass if log_relative_margin is not 0.
            // It could be built differently, but currently we have to take care of it.
            if (log_relative_margin == 0) begin
                testcase_cr;
            end
            testcase_full_random;
            testcase_margin;
        end
        1: testcase_letters;
        2: testcase_numbers;
        3: testcase_space;
        4: testcase_unvalid;
        5: testcase_margin;
        6: testcase_full_random;
        7: testcase_cr;
        default: $error("Testcase %d not supported", testcase);
        endcase

        $display("Sequencer : end");
    endtask : run

endclass : Sequencer


`endif // SEQUENCER_SV
